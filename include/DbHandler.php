<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Nikhil Ramavath
 * @link URL Tutorial link
 */
class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    /* ------------- `users` table method ------------------ */

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function createUser($fb_id,$name,$email, $mobile,$gender,$dob,$ref_status,$ref_number) {
       // require_once 'PassHash.php';
        $response = array();

        // First check if user already existed in db
        if (!$this->isUserExists($fb_id)) {


            // Generating password hash
           // $password_hash = PassHash::hash($password);

            // Generating API key
            $api_key = $this->generateApiKey();

            // insert query
            $stmt = $this->conn->prepare("INSERT INTO users(fb_id,name,email, mobile,gender,dob,ref_number,ref_status,api_key) values(?, ?, ?, ?,?,?,?,?,?)");
            $stmt->bind_param("sssssssss", $fb_id,$name, $email,$mobile,$gender,$dob,$ref_number,$ref_status,$api_key);
          print_r($this->conn->error);
            $result = $stmt->execute();

            $stmt->close();

            // Check for successful insertion
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        } else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }

        return $response;
    }


 /**
     * fetching alerts
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */

 public function fetchalerts($mobile,$fb_id)
 {

    $stmt = $this->conn->prepare("SELECT * FROM `users` WHERE ref_number =".$mobile);
       // $stmt->bind_param("s", $fb_id);
    if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
           /* $stmt->bind_result($id, $source_distance, $destination_distance);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["source_distance"] = $source_distance;
            $user["destination_distance"] = $destination_distance;*/

            $users = $stmt->get_result();
            $stmt->close();
            return $users;
        } else {
            return NULL;
        }
    }


 /**
     * fetching user posted rides
     * @param String $fb_id user facebook id
     
     */

 public function fetchinguserpostedrides($fb_id)
 {

    $stmt = $this->conn->prepare("SELECT * FROM `rides` WHERE fb_id = ".$fb_id);
       // $stmt->bind_param("s", $fb_id);
    if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
           /* $stmt->bind_result($id, $source_distance, $destination_distance);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["source_distance"] = $source_distance;
            $user["destination_distance"] = $destination_distance;*/

            $users = $stmt->get_result();
            $stmt->close();
            return $users;
        } else {
            return NULL;
        }
    }

     /**
     * fetching user  rides
     * @param String $fb_id user facebook id
     
     */

     public function fetchinguserrides($fb_id)
     {

        $stmt = $this->conn->prepare("SELECT user_rides.id as ride_id,user_rides.fb_id as ride_fb_id,user_rides.status,user_rides.task_id as ride_task_id,user_rides.created_at as ride_date,users.*,rides.* FROM `user_rides` inner join users INNER JOIN rides WHERE user_rides.fb_id = ".$fb_id." and users.fb_id = (select fb_id from rides where id = user_rides.task_id) and rides.id = user_rides.task_id");
       // $stmt->bind_param("s", $fb_id);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
           /* $stmt->bind_result($id, $source_distance, $destination_distance);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["source_distance"] = $source_distance;
            $user["destination_distance"] = $destination_distance;*/

            $users = $stmt->get_result();
            $stmt->close();
            return $users;
        } else {
            return NULL;
        }
    }


     /**
     * fetching riders details
     * @param String $ride_id user ride id
     
     */

     public function fetchingridersinfo($ride_id)
     {

        $stmt = $this->conn->prepare("SELECT users.*,user_rides.* FROM `users` inner join user_rides WHERE users.fb_id=user_rides.fb_id and task_id = ".$ride_id);
       // $stmt->bind_param("s", $fb_id);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
           /* $stmt->bind_result($id, $source_distance, $destination_distance);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["source_distance"] = $source_distance;
            $user["destination_distance"] = $destination_distance;*/

            $users = $stmt->get_result();
            $stmt->close();
            return $users;
        } else {
            return NULL;
        }
    }



 /**
     * accepting or cancelling the refernce request
     * @param String $status 2 if accept or 1 if reject
     * @param String $fb_id 
     
     */

     public function acceptorrejectrefernce($status,$fb_id)
     {

        $stmt = $this->conn->prepare("update users set ref_status =".$status." where fb_id = " .$fb_id);
       // $stmt->bind_param("s", $fb_id);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
           /* $stmt->bind_result($id, $source_distance, $destination_distance);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["source_distance"] = $source_distance;
            $user["destination_distance"] = $destination_distance;*/

            
            return true;
        } else {
            return false;
        }
    }


 /**
     * accepting or cancelling the refernce request
     * @param String $status 2 if accept or 1 if reject
     * @param String $fb_id 
     
     */

     public function acceptorrejectride($status,$ride_id)
     {

        $stmt = $this->conn->prepare("update user_rides set user_rides.status ='".$status."' where id = '" .$ride_id."'");

 
       // $stmt->bind_param("s", $fb_id);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
           /* $stmt->bind_result($id, $source_distance, $destination_distance);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["source_distance"] = $source_distance;
            $user["destination_distance"] = $destination_distance;*/

            
            return true;
        } else {
            return false;
        }
    }




 /**
     * fetching ride alerts
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */

 public function fetchridealerts($mobile,$fb_id)
 {

    $stmt = $this->conn->prepare("select users.*,user_rides.id,user_rides.task_id from user_rides INNER JOIN users where user_rides.task_id IN (select id from rides where fb_id = ".$fb_id.") and users.fb_id = user_rides.fb_id ");
       // $stmt->bind_param("s", $fb_id);
    if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
           /* $stmt->bind_result($id, $source_distance, $destination_distance);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["source_distance"] = $source_distance;
            $user["destination_distance"] = $destination_distance;*/

            $users = $stmt->get_result();
            $stmt->close();
            return $users;
        } else {
            return NULL;
        }
    }




    /**
     * Checking user login
     * @param String $email User login email id
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($email, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password_hash FROM users WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->bind_result($password_hash);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return FALSE;
        }
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($fb_id) {


        $stmt = $this->conn->prepare("SELECT fb_id from users WHERE fb_id = ?");
        $stmt->bind_param("s", $fb_id);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email) {
        $stmt = $this->conn->prepare("SELECT name, email, api_key, status, created_at FROM users WHERE email = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($name, $email, $api_key, $status, $created_at);
            $stmt->fetch();
            $user = array();
            $user["name"] = $name;
            $user["email"] = $email;
            $user["api_key"] = $api_key;
            $user["status"] = $status;
            $user["created_at"] = $created_at;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }


     /**
     * Fetching user by fb_id
     * @param String $email User fb id
     */
     public function getUserByFb_id($fb_id) {
        $stmt = $this->conn->prepare("SELECT name, email, api_key, ref_status, created_at,mobile,gender,dob FROM users WHERE fb_id = ?");
        $stmt->bind_param("s", $fb_id);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($name, $email, $api_key, $ref_status, $created_at,$mobile,$gender,$dob);
            $stmt->fetch();
            $user = array();
            $user["name"] = $name;
            $user["email"] = $email;
            $user["gender"] = $gender;
            $user["mobile"]=$mobile;
            $user["dob"]=$dob;
            $user["api_key"] = $api_key;
            $user["ref_status"] = $ref_status;
            $user["created_at"] = $created_at;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyById($user_id) {
        $stmt = $this->conn->prepare("SELECT api_key FROM users WHERE id = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            // $api_key = $stmt->get_result()->fetch_assoc();
            // TODO
            $stmt->bind_result($api_key);
            $stmt->close();
            return $api_key;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getUserId($api_key) {
        $stmt = $this->conn->prepare("SELECT fb_id FROM users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        if ($stmt->execute()) {
            $stmt->bind_result($fb_id);
            $stmt->fetch();
            // TODO
            // $user_id = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $fb_id;
        } else {
            return NULL;
        }
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key) {
 
        $stmt = $this->conn->prepare("SELECT fb_id from users WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }

    /*-----------------`rides` table method--------------------*/

/*SELECT id, 3956 * 2 * ASIN(SQRT( POWER(SIN((12.951304 - source_latitude)*pi()/180/2),2) +COS(12.951304*pi()/180 )*COS(source_latitude*pi()/180) *POWER(SIN((77.705832-source_longitude)*pi()/180/2),2))) as source_distance,3956 * 2 * ASIN(SQRT( POWER(SIN((12.951304 - destination_latitude)*pi()/180/2),2) +COS(12.951304*pi()/180 )*COS(destination_latitude*pi()/180) *POWER(SIN((77.705832-destination_longitude)*pi()/180/2),2))) as destination_distance FROM rides HAVING source_distance <10 and destination_distance <10 ORDER BY source_distance limit 100

*/


 /**
     * Creating new ride
     * @param String $user_id user id to whom task belongs to
     * @param String $task task text
     */
 public function createRide($user_id, $car_model,$seats,$cost,$source_latitude,$source_longitude,$destination_latitude,$destination_longitude,$start_time,$source,$destination,$dateofride,$message) {

$date = date('Y-m-d H:i:s', time());
    $stmt = $this->conn->prepare("INSERT INTO rides(fb_id,car_model,seats,cost,source,source_latitude,source_longitude,destination,destination_latitude,destination_longitude,start_time,dateofride,created_at,message) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $stmt->bind_param("ssssssssssssss", $user_id,$car_model,$seats,$cost,$source,$source_latitude,$source_longitude,$destination,$destination_latitude,$destination_longitude,$start_time,$dateofride,$date,$message);
    $result = $stmt->execute();
    $stmt->close();

    if ($result) {

        return $this->conn->insert_id;
    } else {
            // task failed to create
        return NULL;
    }
}



 /**
     * Function to assign a ride to user
     * @param String $user_id id of the user
     * @param String $task_id id of the ride
     */
 public function createUserRide($user_id, $task_id,$message) {

$date = date('Y-m-d H:i:s', time());
    $stmt = $this->conn->prepare("INSERT INTO user_rides(fb_id, task_id,created_at,message) values(?, ?,?,?)");
    $stmt->bind_param("siss", $user_id, $task_id,$date,$message);
    $result = $stmt->execute();


    $stmt->close();
    if ($result) {

        return $this->conn->insert_id;
    } else {
            // task failed to create
        return NULL;
    }

}

/**
* get fcm_id by using fb_id
* @param String $fb_id 
*/

public function fetchFcmId($fb_id)
{

  $stmt = $this->conn->prepare("SELECT fcm_id FROM users WHERE fb_id = ?");
  $stmt->bind_param("s", $fb_id);
  if ($stmt->execute()) {
    $stmt->bind_result($fcm_id);
    $stmt->fetch();
            // TODO
            // $user_id = $stmt->get_result()->fetch_assoc();
    $stmt->close();
    return $fcm_id;
} else {
    return NULL;
}

}


/**
* get fcm_id by using fb_id
* @param String $fb_id 
*/

public function getFcmIdByID($user_id)
{

  $stmt = $this->conn->prepare("SELECT fcm_id FROM users WHERE id = ?");
  $stmt->bind_param("s", $user_id);
  if ($stmt->execute()) {
    $stmt->bind_result($fcm_id);
    $stmt->fetch();
            // TODO
            // $user_id = $stmt->get_result()->fetch_assoc();
    $stmt->close();
    return $fcm_id;
} else {
    return NULL;
}

}



/**
    * Function to get rider providers with in 5 km radius of ride needer
    * @param String $source_latitiude source latitude of the user
    * @param String $source_longitutde source longitutde of the user
    * @param String $destination_latitude destination latitude of the user
    * @param String $destination_longitutde destination longitutde of the user

*/

public function getNearByRiders($user_id,$source_latitude,$source_longitude,$destination_latitude,$destination_longitutde,$dateofride)
{
$date = date('Y-m-d H:i:s', time());

//use this query if you want to remove user's own rides

   /* $stmt = $this->conn->prepare("SELECT users.*,rides.*, 3956 * 2 * ASIN(SQRT( POWER(SIN(($source_latitude - source_latitude)*pi()/180/2),2) +COS($source_latitude*pi()/180 )*COS(source_latitude*pi()/180) *POWER(SIN(($source_longitude-source_longitude)*pi()/180/2),2))) as source_distance,3956 * 2 * ASIN(SQRT( POWER(SIN(($destination_latitude - destination_latitude)*pi()/180/2),2) +COS($destination_latitude*pi()/180 )*COS(destination_latitude*pi()/180) *POWER(SIN(($destination_longitutde-destination_longitude)*pi()/180/2),2))) as destination_distance FROM rides INNER JOIN users ON rides.fb_id = users.fb_id HAVING source_distance <3 and destination_distance <3 and rides.fb_id!=$user_id and rides.dateofride = '".$dateofride."' and rides.start_time > '".$date."'ORDER BY source_distance limit 100");*/

//use this query if you want to show user's own rides
   $stmt = $this->conn->prepare("SELECT users.*,rides.*, 3956 * 2 * ASIN(SQRT( POWER(SIN(($source_latitude - source_latitude)*pi()/180/2),2) +COS($source_latitude*pi()/180 )*COS(source_latitude*pi()/180) *POWER(SIN(($source_longitude-source_longitude)*pi()/180/2),2))) as source_distance,3956 * 2 * ASIN(SQRT( POWER(SIN(($destination_latitude - destination_latitude)*pi()/180/2),2) +COS($destination_latitude*pi()/180 )*COS(destination_latitude*pi()/180) *POWER(SIN(($destination_longitutde-destination_longitude)*pi()/180/2),2))) as destination_distance FROM rides INNER JOIN users ON rides.fb_id = users.fb_id HAVING source_distance <3 and destination_distance <3  and rides.dateofride = '".$dateofride."' and rides.start_time > '".$date."'ORDER BY source_distance limit 100");




       // $stmt->bind_param("s", $fb_id);
    if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
           /* $stmt->bind_result($id, $source_distance, $destination_distance);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["source_distance"] = $source_distance;
            $user["destination_distance"] = $destination_distance;*/

            $users = $stmt->get_result();
            $stmt->close();
            return $users;
        } else {
            return NULL;
        }

    }



/**
    * Function to get rider providers with in 5 km radius of ride needer
    * @param String $source_latitiude source latitude of the user
    * @param String $source_longitutde source longitutde of the user
    * @param String $destination_latitude destination latitude of the user
    * @param String $destination_longitutde destination longitutde of the user

*/

public function fetchingusercompletedetails($fb_id)
{
$date = date('Y-m-d H:i:s', time());

    $stmt = $this->conn->prepare("SELECT count(users.ref_number)as refernce_count ,(SELECT COUNT(rides.id) from rides WHERE rides.fb_id = ".$fb_id." ) as rideposted_count ,(SELECT COUNT(user_rides.id) FROM user_rides WHERE user_rides.fb_id = ".$fb_id." and user_rides.status = '2' ) as ridetaken_count FROM `users` WHERE users.ref_number = (SELECT users.mobile FROM users WHERE users.fb_id = ".$fb_id.")");
       // $stmt->bind_param("s", $fb_id);
    if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
           /* $stmt->bind_result($id, $source_distance, $destination_distance);
            $stmt->fetch();
            $user = array();
            $user["id"] = $id;
            $user["source_distance"] = $source_distance;
            $user["destination_distance"] = $destination_distance;*/

            $users = $stmt->get_result();
            $stmt->close();
            return $users;
        } else {
            return NULL;
        }

    }



/**
* get fcm_id for refernce number
* @param ref_number
*/

public function getFcmIdByRefNumber($ref_number)
{

 
    $stmt = $this->conn->prepare("SELECT fb_id,fcm_id from users where mobile= ?");
    $stmt->bind_param("s", $ref_number);
    if ($stmt->execute()) {
        $res = array();
       /* $stmt->bind_result($fb_id,$fcm_id);
            // TODO
            // $task = $stmt->get_result()->fetch_assoc();
        print_r($fb_id);
        $stmt->fetch();
        $res["fcm_id"] = $fcm_id;
        $res["fb_id"] = $fb_id;*/
        $res = $stmt->get_result()->fetch_assoc();
        $stmt->close();
        return $res;
    } else {
        return NULL;
    }
} 





/* ------------- `tasks` table method ------------------ */

    /**
     * Creating new task
     * @param String $user_id user id to whom task belongs to
     * @param String $task task text
     */
    public function createTask($user_id, $task) {
        $stmt = $this->conn->prepare("INSERT INTO tasks(task) VALUES(?)");
        $stmt->bind_param("s", $task);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            // task row created
            // now assign the task to user
            $new_task_id = $this->conn->insert_id;
            $res = $this->createUserTask($user_id, $new_task_id);
            if ($res) {
                // task created successfully
                return $new_task_id;
            } else {
                // task failed to create
                return NULL;
            }
        } else {
            // task failed to create
            return NULL;
        }
    }

    /**
     * Fetching single task
     * @param String $task_id id of the task
     */
    public function getTask($task_id, $user_id) {
        $stmt = $this->conn->prepare("SELECT t.id, t.task, t.status, t.created_at from tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $task_id, $user_id);
        if ($stmt->execute()) {
            $res = array();
            $stmt->bind_result($id, $task, $status, $created_at);
            // TODO
            // $task = $stmt->get_result()->fetch_assoc();
            $stmt->fetch();
            $res["id"] = $id;
            $res["task"] = $task;
            $res["status"] = $status;
            $res["created_at"] = $created_at;
            $stmt->close();
            return $res;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching all user tasks
     * @param String $user_id id of the user
     */
    public function getAllUserTasks($user_id) {
        $stmt = $this->conn->prepare("SELECT t.* FROM tasks t, user_tasks ut WHERE t.id = ut.task_id AND ut.user_id = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $tasks = $stmt->get_result();
        $stmt->close();
        return $tasks;
    }

/** 
* updating FCM ID
* @param String $fcm_id of the user
*/

public function updateFcmID($user_id, $fcm_id)
{
//update users set fcm_id = "2" where api_key="2c0a440846e387da745df4eb65f07772"

 $stmt = $this->conn->prepare("UPDATE users  set fcm_id = ? WHERE fb_id = ?");
 $stmt->bind_param("ss", $fcm_id, $user_id);
 $stmt->execute();
 $num_affected_rows = $stmt->affected_rows;
 $stmt->close();
 return $num_affected_rows > 0;


}

/**
* updating number of seats availabe by checking number of seats available
* @param String ride_id,user_id
*/
public function updateseatsavailable($ride_id,$fb_id){
//update rides set `seats_available` = `seats_available` + 1 where `seats_available` <`seats` and id =34
   $stmt = $this->conn->prepare("UPDATE rides SET `seats_available` = `seats_available` + 1 WHERE `seats_available` <`seats` AND id= (SELECT user_rides.task_id FROM user_rides WHERE id = ?)");
   $stmt->bind_param("s", $ride_id);
   $stmt->execute();
   $num_affected_rows = $stmt->affected_rows;
   $stmt->close();
   return $num_affected_rows > 0;

}




    /**
     * Updating task
     * @param String $task_id id of the task
     * @param String $task task text
     * @param String $status task status
     */
    public function updateTask($user_id, $task_id, $task, $status) {
        $stmt = $this->conn->prepare("UPDATE tasks t, user_tasks ut set t.task = ?, t.status = ? WHERE t.id = ? AND t.id = ut.task_id AND ut.user_id = ?");
        $stmt->bind_param("siii", $task, $status, $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    /**
     * Deleting a task
     * @param String $task_id id of the task to delete
     */
    public function deleteTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("DELETE t FROM tasks t, user_tasks ut WHERE t.id = ? AND ut.task_id = t.id AND ut.user_id = ?");
        $stmt->bind_param("ii", $task_id, $user_id);
        $stmt->execute();
        $num_affected_rows = $stmt->affected_rows;
        $stmt->close();
        return $num_affected_rows > 0;
    }

    /* ------------- `user_tasks` table method ------------------ */

    /**
     * Function to assign a task to user
     * @param String $user_id id of the user
     * @param String $task_id id of the task
     */
    public function createUserTask($user_id, $task_id) {
        $stmt = $this->conn->prepare("INSERT INTO user_tasks(user_id, task_id) values(?, ?)");
        $stmt->bind_param("ii", $user_id, $task_id);
        $result = $stmt->execute();

        if (false === $result) {
            die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
        $stmt->close();
        return $result;
    }


      /**
     * Function to assign a task to user
     * @param String $user_id id of the user
     * @param String $task_id id of the task
     */
    public function createContactus($type, $message,$user_id) {
        
      
        $stmt = $this->conn->prepare("INSERT INTO contactus(fb_id, type,message) values(?, ?,?)");
//print_r("INSERT INTO contactus(fb_id, type,message) values(".$user_id.",".$type.",".$message.")");
        $stmt->bind_param("sss", $user_id, $type,$message);
        $result = $stmt->execute();
       

        if (false === $result) {
           // die('execute() failed: ' . sqlerror());
        }
        $new_task_id = $this->conn->insert_id;
        $stmt->close();
        return $new_task_id;
    }


/** Function to update attachment of an contact us query
* @param String $attachment_url url of the attachment
* @param String $task_id id of the task
*/

public Function updateAttachmentUrl($attachment_url,$task_id)
{
      $stmt = $this->conn->prepare("UPDATE contactus set contactus.attachment_url = ? where contactus.id = ?");
        $stmt->bind_param("ss", $attachment_url,$task_id);
        $result = $stmt->execute();

        if (false === $result) {
            die('execute() failed: ' . htmlspecialchars($stmt->error));
        }
        $stmt->close();
        return $result; 
}

}

?>
